package hu.ngms.chapel.projectwizard.ui;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Widget;

public class EnvironmentVariableEditor extends FieldEditor {

	private Composite buttonBox;
	private Table table;
	private Button addButton;
	private Button removeButton;
	private Button editButton;

	private SelectionListener selectionListener;

	public EnvironmentVariableEditor(String name, String labelText, Composite parent) {
		super(name, labelText, parent);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void adjustForNumColumns(int arg0) {
		Control control = getLabelControl();
		((GridData) control.getLayoutData()).horizontalSpan = arg0;
		((GridData) table.getLayoutData()).horizontalSpan = arg0 - 1;

	}

	@Override
	protected void doFillIntoGrid(Composite arg0, int arg1) {

		Control control = getLabelControl(arg0);
		GridData gd = new GridData();
		gd.horizontalSpan = arg1;
		control.setLayoutData(gd);

		// Get Table control
		table = getTableControl(arg0);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.verticalAlignment = GridData.FILL;
		gd.horizontalSpan = arg1 - 1;
		gd.grabExcessHorizontalSpace = true;
		table.setLayoutData(gd);

		// Get button control
		buttonBox = getButtonBoxControl(arg0);
		gd = new GridData();
		gd.verticalAlignment = GridData.BEGINNING;
		buttonBox.setLayoutData(gd);

	}

	private Table getTableControl(Composite parent) {
		if (table == null) {
			table = new Table(parent, SWT.SINGLE | SWT.BORDER | SWT.FULL_SELECTION);
			table.setLinesVisible(true);

			TableColumn tableColumn = new TableColumn(table, SWT.CENTER);
			tableColumn.setText("Name");
			TableColumn tableColumn2 = new TableColumn(table, SWT.CENTER);
			tableColumn2.setText("Value");
			table.setHeaderVisible(true);

			for (int i = 0; i < 2; i++) {
				table.getColumn(i).pack();
			}

			table.addSelectionListener(getSelectionListener());
		}
		return table;
	}

	private Composite getButtonBoxControl(Composite parent) {
		if (buttonBox == null) {
			buttonBox = new Composite(parent, SWT.NULL);
			GridLayout layout = new GridLayout();
			layout.marginWidth = 0;
			buttonBox.setLayout(layout);
			createButtons(buttonBox);
			buttonBox.addDisposeListener(new DisposeListener() {
				public void widgetDisposed(DisposeEvent event) {
					addButton = null;
					removeButton = null;
					editButton = null;
					buttonBox = null;
				}
			});

		} else {
			checkParent(buttonBox, parent);
		}

		selectionChanged();
		return buttonBox;
	}

	private void createButtons(Composite box) {
		addButton = createPushButton(box, "Add...");//$NON-NLS-1$
		editButton = createPushButton(box, "Edit...");//$NON-NLS-1$
		removeButton = createPushButton(box, "Remove");//$NON-NLS-1$
	}

	private Button createPushButton(Composite parent, String key) {
		Button button = new Button(parent, SWT.PUSH);
		button.setText(key);
		button.setFont(parent.getFont());
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		int widthHint = convertHorizontalDLUsToPixels(button, IDialogConstants.BUTTON_WIDTH);
		data.widthHint = Math.max(widthHint, button.computeSize(SWT.DEFAULT, SWT.DEFAULT, true).x);
		button.setLayoutData(data);
		button.addSelectionListener(getSelectionListener());
		return button;
	}

	private SelectionListener getSelectionListener() {
		if (selectionListener == null) {
			createSelectionListener();
		}
		return selectionListener;
	}

	public void createSelectionListener() {
		selectionListener = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				Widget widget = event.widget;
				if (widget == addButton) {
					addPressed();
				} else if (widget == removeButton) {
					removePressed();
				} else if (widget == editButton) {
					editPressed();
				} else if (widget == table) {
					selectionChanged();
				}
			}

		};
	}

	private void selectionChanged() {

		removeButton.setEnabled(table.getSelectionCount() > 0);
		editButton.setEnabled(table.getSelectionCount() > 0);

	}

	private void removePressed() {
		if (table.getSelectionCount() > 0) {
			table.remove(table.getSelectionIndex());
		}
	}

	private void editPressed() {
		if (table.getSelectionCount() > 0) {
			TableItem selectedRow = table.getSelection()[0];
			EnvironmentVariableDialog dialog = new EnvironmentVariableDialog(Display.getDefault().getActiveShell());
			dialog.setVariableName(selectedRow.getText(0));
			dialog.setVariableValue(selectedRow.getText(1));
			if (dialog.open() == IDialogConstants.OK_ID) {
				selectedRow.setText(new String[] { dialog.getVariableName(), dialog.getVariableValue() });
			}
		}

	}

	private void addPressed() {
		EnvironmentVariableDialog dialog = new EnvironmentVariableDialog(Display.getDefault().getActiveShell());
		if (dialog.open() == IDialogConstants.OK_ID) {
			TableItem newItem = new TableItem(table, SWT.NONE);
			newItem.setText(new String[] { dialog.getVariableName(), dialog.getVariableValue() });
		}
	}

	// Name;Value#Name;Value
	@Override
	protected void doLoad() {
		if (table != null) {
			String s = getPreferenceStore().getString(getPreferenceName());
			if (!s.isEmpty()) {
				String[] array = s.split("#");
				for (int i = 0; i < array.length; i++) {
					TableItem item = new TableItem(table, SWT.NONE);
					String[] row = array[i].split(";");
					item.setText(new String[] { row[0], row[1] });
				}
			}
		}

	}

	@Override
	protected void doLoadDefault() {
		if (table != null) {
			table.removeAll();
			doLoad();
		}

	}

	@Override
	protected void doStore() {
		if (table != null && table.getItemCount() > 0) {
			StringBuilder builder = new StringBuilder();
			for (TableItem item : table.getItems()) {
				builder.append(item.getText(0));
				builder.append(";");
				builder.append(item.getText(1));
				builder.append("#");
			}

			if (builder.length() > 0) {
				getPreferenceStore().setValue(getPreferenceName(), builder.toString());
			}
		}

	}

	@Override
	public int getNumberOfControls() {
		return 2; // Table and buttonboxcontrol
	}

}
