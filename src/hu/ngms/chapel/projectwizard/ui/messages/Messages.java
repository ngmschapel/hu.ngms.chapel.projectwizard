package hu.ngms.chapel.projectwizard.ui.messages;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

	// Keys
	public static String ChapelProjectWizard_title;
	public static String ChapelProjectWizard_project;
	public static String ChapelProjectWizard_description;
	public static String ChapelPreferencePage_label;

	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}

	private Messages() {
	}

}
