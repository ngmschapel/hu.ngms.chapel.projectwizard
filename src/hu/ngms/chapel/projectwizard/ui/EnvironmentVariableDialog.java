package hu.ngms.chapel.projectwizard.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.widgets.Shell;

public class EnvironmentVariableDialog extends Dialog {

	private Text txtVariableName;
	private Text txtVariableValue;

	private String variableName;
	private String variableValue;

	public EnvironmentVariableDialog(Shell parent) {
		super(parent);
	}

	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Environment variables");
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout(2, false);
		layout.marginRight = 5;
		layout.marginLeft = 10;
		container.setLayout(layout);

		Label lblVariableName = new Label(container, SWT.NONE);
		lblVariableName.setText("Name:");

		txtVariableName = new Text(container, SWT.BORDER);
		txtVariableName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		if (variableName != null) {
			txtVariableName.setText(variableName);
		}
		txtVariableName.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				Text textWidget = (Text) e.getSource();
				variableName = textWidget.getText();
			}
		});

		Label lblVariableValue = new Label(container, SWT.NONE);
		GridData gd_lblNewLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblNewLabel.horizontalIndent = 1;
		lblVariableValue.setLayoutData(gd_lblNewLabel);
		lblVariableValue.setText("Value:");

		txtVariableValue = new Text(container, SWT.BORDER);
		txtVariableValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		if (variableValue != null) {
			txtVariableValue.setText(variableValue);
		}
		txtVariableValue.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				Text textWidget = (Text) e.getSource();
				variableValue = textWidget.getText();
			}
		});
		return container;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Point getInitialSize() {
		return new Point(450, 150);
	}

	@Override
	protected void okPressed() {
		variableName = txtVariableName.getText();
		variableValue = txtVariableValue.getText();
		super.okPressed();
	}

	public String getVariableName() {
		return variableName.toUpperCase();
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName.toUpperCase();
	}

	public String getVariableValue() {
		return variableValue;
	}

	public void setVariableValue(String variableValue) {
		this.variableValue = variableValue;
	}

}
