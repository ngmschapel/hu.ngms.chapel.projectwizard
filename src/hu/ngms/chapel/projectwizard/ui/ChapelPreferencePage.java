package hu.ngms.chapel.projectwizard.ui;

import hu.ngms.chapel.projectwizard.Activator;
import hu.ngms.chapel.projectwizard.ui.messages.Messages;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class ChapelPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
	}

	@Override
	protected void createFieldEditors() {

		FileFieldEditor fileEditor = new FileFieldEditor("Compiler path", Messages.ChapelPreferencePage_label,
				getFieldEditorParent());
		addField(fileEditor);
		EnvironmentVariableEditor environmentVariablesTable = new EnvironmentVariableEditor("Environment variables",
				"Environment variables", getFieldEditorParent());
		addField(environmentVariablesTable);
	}

}
